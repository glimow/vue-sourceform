# vue-sourceform

Simple Library for vuejs form generation from Javascript code. `vue-sourceform` aims at producing nice and reusable Vuejs forms from JSON-like schemas with possibility to integrate custom hooks and components. Uses at-ui by default.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Contributors

Tristan Kalos (tristan.kalos@sourced.tech)
