import Vue from "vue";
import App from "./App.vue";

import AtComponents from "at-ui";
import "at-ui-style"; // Import CSS

import "prismjs";
import "prismjs/themes/prism.css";
import VuePrismEditor from "vue-prism-editor";
import "vue-prism-editor/dist/VuePrismEditor.css";

// import 'at-ui-style/src/index.scss'      // Or import the unbuilt version of SCSS

Vue.use(AtComponents);
Vue.component("prism-editor", VuePrismEditor);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
